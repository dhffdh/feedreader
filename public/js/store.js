/*jshint unused:false */

(function (exports) {

	'use strict';

	var CHANNELS_KEY = 'channels';
	var NEWS_KEY = 'news';

	exports.feedsStorage = {
		fetch: function () {
			return JSON.parse(localStorage.getItem(CHANNELS_KEY) || '[]');
		},
		save: function (channels) {
			localStorage.setItem(CHANNELS_KEY, JSON.stringify(channels));
		}
	};

	exports.newsStorage = {
		fetch: function (_id) {
			var id = localStorage.getItem(NEWS_KEY+'_id');
			if (id == _id) {
				return JSON.parse(localStorage.getItem(NEWS_KEY) || '[]');
			}else{
				return false;
			}
		},
		save: function ( _news, _id) {
			localStorage.setItem(NEWS_KEY+'_id', _id);
			localStorage.setItem(NEWS_KEY, JSON.stringify(_news));
		}
	};

})(window);
