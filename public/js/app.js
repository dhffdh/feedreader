
(function (exports) {
	'use strict';

	var apiUrlChannels = "/feed_channels";
	var apiUrlNews = '/channel_news';

	// Главный комопнент-родитель
	var App = Vue.extend({
		data: function(){
			return{
				feed_channels: []
			}
		},
		ready: function(){
			this.get_channels();
		},
		watch: {
			feed_channels: {
				handler: function (_feed_channels) {
				  feedsStorage.save(_feed_channels);
				},
				deep: true
			}
		},
		computed: {},

		methods: {
			//Получаем список каналов
			get_channels: function(){
				this.feed_channels = feedsStorage.fetch();
				if (this.feed_channels.length == 0){
					this.refresh_channels();
				}
				return true;
			},
			// Оновление каналов с сервера
			refresh_channels: function(){

				this.$http.get(apiUrlChannels,null,{timeout: 3000}).then(function(res){
					//console.log(res);
					if (typeof( res.data) === 'object' ) {
						this.feed_channels = res.data;
					};
					router.go({name: 'home'});
					return true;
				},function(res){
					//error callback
					console.log(res.status);
					return false;
				});
			},
			// Синхронизация каналов на сервер
			sync_channels: function(){
				for (var i in this.feed_channels) {
					//Перед отправкой заново проставляем id
					this.feed_channels[i].id = +i+1;
				}


				this.$http.put(apiUrlChannels, JSON.stringify(this.feed_channels),{timeout: 3000} ).then(function(res){
					//console.log(res);
					if (typeof( res.data) === 'object' ) {
						this.feed_channels = res.data;
					};
					router.go({name: 'home'});
					return true;
				},function(res){
					console.log(res.data);
					return false;
				});
			},
			add_channel: function(_url){
				this.feed_channels.push({
		    		id: (this.feed_channels.length+1),
					url: _url, 
					title: 'empty',
					desc: 'empty'
				});
				this.sync_channels();
				return true;
			},
			get_channel: function(_id){
				for (var i in this.feed_channels) {
					if (this.feed_channels[i].id == _id) {
						//console.log(this.feed_channels[i].id);
						return this.feed_channels[i];
						break;
					}
				}
				return false;
		    },
		    set_channel_url: function(_id, _url){
				for (var i in this.feed_channels) {
					if (this.feed_channels[i].id == _id) {
						//console.log(this.feed_channels[i].id);
						this.feed_channels[i].url = _url;
						this.sync_channels();
						return true;
						break;
					}
				}
				return false;
		    },
		    delete_channel: function(_id){
		    	for (var i in this.feed_channels) {
					if (this.feed_channels[i].id == _id) {
						this.feed_channels.splice(i, 1);
						this.sync_channels();

						return true;
						break;
					}
				}
				return false;
		    }
		},

		directives: {},

		events: {
		    'add-channel': function (url) {
		    	this.add_channel(url);
		    },
		    'delete-channel': function (id) {
		    	if( this.delete_channel(id) ){
		    		//console.log('Успешно удалено');
		    	}else{
		    		alert('Не найдено');
		    	}
		    }
		}
	});



	// Компонент добавления канала
	var AddChannel = Vue.extend({
		template: '#add',
		data: function(){
			return{
				new_url_channel: ''
			};
		},
		methods: {
			add_channel: function (e) {

				this.$validate(true)
				if (this.$validation_url.invalid) {
					e.preventDefault()
				}else{

					if (this.new_url_channel.trim()) {
						if (!this.new_url_channel){
							return;
						}
						this.$dispatch('add-channel', this.new_url_channel);
						this.new_url_channel = '';
					}
				}
			}
		}
	});

	// Компонент редактирования канала
	var EditChannel = Vue.extend({
		template: '#edit',
		data: function(){
			return{
				feed_channel: null
			};
		},
		ready: function(){
			this.get_channel();
		},
		methods:{
			get_channel: function(){
				if(this.feed_channel = this.$parent.get_channel( this.$route.params.id ) )
					return true;
				else
					alert('Ошибка, данного канала не существует!');
			},
			edit_channel: function(){

				this.$validate(true)
				if (this.$validation_url.invalid) {
					e.preventDefault()
				}else{
					
					if( this.$parent.set_channel_url( this.feed_channel.id ,  this.feed_channel.url ) )
					{
						router.go({
							name: 'feed',
							params: { id: this.$route.params.id 
							}
						});
					}
					else
						alert('Ошибка редактирования!');
				
				}
			}
		},
		route: {
		    canReuse: false
		}
	});


	// Компонент модального окна Удаления канала
	var DeleteChannelModal = Vue.extend({
		template: '#delete_modal',
		props: {
			show: {
				type: Boolean,
				required: true,
				twoWay: true
			}
		},
		methods:{
			call_delete_channel: function(){
				this.$dispatch('call-delete-channel');
				this.show = false;
			}
		}
	});


	var filters = {
		all: function (feed_links) {
			return feed_links;
		},
		active: function (feed_links) {
			return feed_links.filter(function (link) {
				return !link.viewed;
			});
		}
	};

	var filters_pager = function(feed_links , page_id , size){
		//return feed_links;
		//console.log('page: '+page_id+' ,size: '+size);

		return feed_links.slice( ((+page_id-1)*size), ((+page_id)*size) ) ;
	};
	// Компонент просмотра канала
	var ViewChannel = Vue.extend({
		template: '#view',
		data: function(){
			return{
				feed_channel: null,
				feed_links: [],
				showDeleteModal: false, // Переключатель показа формы удаления
				visibility: 'all',	//Начальный фильтр показа нвостей
				error_mes: false,
				page: 1,
				page_size: 5,
				pagination: [],
				show_page_size_modal: false
			};
		},
		ready: function(){
			this.get_channel();
		},
		watch: {
			feed_links: {
				handler: function (_news) {
					newsStorage.save( _news , this.feed_channel.id );
					
				},
				deep: true
			},
			page_size: {
				handler: function(size){
					this.build_pagination();
				}
			}
		},
		computed: {
			filteredLinks_on_page: function () {
				return filters_pager(this.filteredLinks,this.page,this.page_size);
			},
			filteredLinks: function () {
				return filters[this.visibility](this.feed_links);
			},
			virtual_pagination: function(){
				//return get_virtual_pagination(this.pagination);
				if (this.pagination.length < 7) {
					return this.pagination;
				}else{
					
					var pag = [];
					if (this.page < 5 ){
						pag = this.pagination.slice(0,5);
						pag.push( {page_id: 6 , title: '...' } ); 
						pag.push( this.pagination[this.pagination.length-1] );

					}else{
						if(this.page < ( this.pagination.length - 4) ){

							pag = this.pagination.slice( (+this.page-4) , (+this.page+3) );
							pag.push( {page_id: (+this.page+4) , title: '...' } ); 
							pag.push( this.pagination[this.pagination.length-1] );

							pag.unshift( {page_id: (+this.page-4) , title: '...' } ); 
							pag.unshift( this.pagination[0] );

						}else{
							pag = this.pagination.slice( (this.pagination.length-6) , this.pagination.length );
							pag.unshift( {page_id: (this.pagination.length-5) , title: '...' } ); 
							pag.unshift( this.pagination[0] );

						}
						
						//pag.push( this.pagination.slice( (+this.page-3) , (+this.page+3) ) );

						//pag.push( this.pagination[ this.pagination.length - 1 ] );
					}
					return pag;
				}
			}
		},
		methods:{
			get_channel: function(){
				if(this.feed_channel = this.$parent.get_channel( this.$route.params.id ) ){
					this.get_channel_news();
					return true;
				}
				else{
					alert('Ошибка, данного канала не существует!');
					router.go({name: 'home'});
				}
			},
			get_channel_news_from_server: function(){

				this.$http.get(apiUrlNews+'/'+this.feed_channel.id,'',{timeout: 2000}).then(function(res){
					//console.log(res);
					if (typeof( res.data) === 'object' ) {
						this.feed_links = res.data;
						this.build_pagination();
					};
				},function(res){
					//error callback
					this.error_mes = 'Ошибка сервера';
					console.log(res.status);
					return false;
				});

			},
			get_channel_news: function(){
				//console.log('get news');

				this.feed_links = newsStorage.fetch( this.feed_channel.id );
				this.build_pagination();
				//console.log(this.feed_links);

				if ( !this.feed_links || this.feed_links.length == 0){
					//console.log('Новостей этого канала пока еще нет');
					this.get_channel_news_from_server();
				}

				return true;
			},
			delete_channel: function(){
				this.$dispatch('delete-channel', this.$route.params.id);
				router.go({name: 'home'});
			},
			build_pagination: function(){
				var len = this.feed_links.length;
				var size = this.page_size;
				if( len > 0 ){
					var items = Math.ceil(len/size);
					//console.log(len);
					//console.log(size);
					//console.log(  items );
					this.pagination = [];

					for (var i = 0; i < items; i++) {
						this.pagination.push({
							page_id: +i+1,
							title: +i+1
						});
					};
					
				}
			},
			refresh_channel_news: function(){
				this.page = 1;
				this.refresh_channel_props();
				this.get_channel_news_from_server();
			},
			refresh_channel_props: function(){
				this.page = 1;
				this.feed_links = [];
				this.showDeleteModal = false;
				this.show_page_size_modal = false;
				this.pagination = [];
				this.error_mes = false;
				this.visibility = 'all';
			}
		},
		events: {
		    'call-delete-channel': function () {
		    	this.delete_channel();
		    }
		},

		route: {
			canReuse: function () {
				return false;
			}
		},
		components: {
			'delete-component': DeleteChannelModal
		}
	});




	// Компонент Главного экрана
	var Home = Vue.extend({
	    template: '<h1>Welcome to RSS-reader</h1>'
	});

	var notfound = Vue.extend({
	   	template: '<div><h1>Not Found</h1></div>'
	});

	//Кастомный валидатор URL
	Vue.validator('url', function (val) {
	  return /^(http\:\/\/|https\:\/\/)(.{4,})$/.test(val)
	})


	var router = new VueRouter({
		hashbang: false,
		linkActiveClass: 'active'
	});

	router.redirect({
	  '/feed': '/',
	  '/edit': '/'
	});


	router.map({
		'*': {
			component: notfound
		},
		'/': {
			name: 'home',
		    component: Home
		},
		'/add': {
			name: 'add',
		    component: AddChannel
		},
		'/feed/:id': {
			name: 'feed',
			component: ViewChannel
		},
		'/edit/:id': {
			name: 'edit',
			component: EditChannel
		}
	})

	router.start(App,'#app');


})(window);