var express = require('express');
var app = express();
var feedparser = require('ortoo-feedparser');
var async = require('async');
var bodyParser = require('body-parser');

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());

app.get('/', function(req, res){
    res.render('index.jade');
});


//Начальный набор данных каналов
var channels = [
	{
		id: 1,
		url: 'https://geektimes.ru/rss/hub/health/', 
	},
	{
		id: 2,
		url: 'https://lenta.ru/rss/news', 
	},
	{
		id: 3,
		url: 'http://news.ngs.ru/rss/'
	},
	{
		id: 4,
		url: 'https://meduza.io/rss/all', 
	},
	{
		id: 5,
		url: 'https://russian.rt.com/rss', 
	}
];

//Получение каналов
var get_channels = function(_res,_channels){

	// Асинхронный парсинг каналов с последущей отправкой на сервер
	async.map(_channels, function(item, callback){ 

		if ( /^(http\:\/\/|https\:\/\/)(.{4,})$/.test(item.url) ) {
			//console.log('Right url!!!');

			feedparser.parseUrl( item.url , {dateformat: 'MM-DD-YYYY'} )
			.on('error', function(error){
				console.log(error.message);
				var channel = {};
				channel.id = item.id;
				channel.url = item.url;
				channel.error = error.message;
				return callback(null,channel);
			})
			.on('meta', function(meta){
				//console.log('meta: ',meta.title);
				//console.log(i);
				var channel = {};
				channel.id = item.id;
				channel.url = item.url;
				channel.title = meta.title;
				channel.error = null;
				channel.desc = meta.description;
				
				if (meta.image.url){
					channel.pic = meta.image.url;
				}
				return callback(null,channel);
			})
		}else{
			//console.log('not url');
			var channel = {};
			channel.id = item.id;
			channel.url = item.url;
			channel.error = 'Invalid URL: '+item.url;
			return callback(null,channel);
		}

	}, function(err, results){
	    // results is now an array of stats for each file
	    _channels = results;
	    _res.json(_channels);
	});
}

//Получение нвоостей с канала
var get_channel_news = function(_res,_channel){

	if ( /^(http\:\/\/|https\:\/\/)(.{4,})$/.test(_channel.url) ) { //проверка URL

		var news_list = [];
		var error = false;
		feedparser.parseUrl( _channel.url , {dateformat: 'MM-DD-YYYY'} )
		.on('error', function(_error){
			console.log(_error.message);
			error = _error.message
		})
		.on('article', function(article){
			//console.log(article.pubdate);
			
			var item = {};
			item.url = article.link;
			item.title = article.title;
			item.desc = article.description;
			item.pic = article.image;
			item.viewed = false;

			news_list.push(item);
		})
		.on('end',function(){
			if (!error)
				_res.json(news_list);
			else{
				_res.status(404).send(error);
			}
		});

	}else{
		console.log('not url');
		_res.json(null);
	}
}



app.get('/feed_channels', function (req, res) {
	console.log('GET request: /feed_channels');

	get_channels(res,channels);

});

app.put('/feed_channels', function (req, res) {
	console.log('PUT request: /feed_channels');
	if (typeof(req.body) === 'object' ) {
		channels = req.body;
		get_channels(res,channels);
	};
});

app.get('/channel_news/:id', function (req, res) {
	
	var id = req.params.id;
	console.log('GET request: /channel_news/'+id);
	//console.log(channels);

	for (var i in channels) {
		if (channels[i].id == id) {
			//console.log(channels[i]);
			get_channel_news(res,channels[i]);
			break;
		}
	}
});

app.listen(3000);
console.log("Server running on port 3000");